public class OpporunityProduct_Controller {
    public List<Opportunity_Product__c> objs {get; set;}
    public List<Opportunity_Product__c> obj {get; set;}
    public String search{get; set;}
    public Integer totalCount{get; set;}
    public Integer count{get; set;}
    public Integer starting{get; set;}
    public Integer ending{get; set;}
    public List<Integer> index{get; set;}
    
    public OpporunityProduct_Controller() {
        index = new List<Integer>{1,2,3,4,5};
        objs = [SELECT Name, Active__c, Primary__c, Opportunity__c, Opportunity__r.Name FROM Opportunity_Product__c];
        totalCount = objs.size();
        FirstPage();
    }
    
    public PageReference save() {
        Map<ID, Opportunity> opps = new Map<ID, Opportunity>();
        for(Opportunity_Product__c one : objs){
            Opportunity opp = new Opportunity(ID=one.Opportunity__c, Name=one.Opportunity__r.Name, CloseDate=Date.today());
            opps.put(one.Opportunity__c, opp);
        }
        UPDATE objs;
        UPDATE opps.values();
        objs = [SELECT Name,Active__c,Primary__c,Opportunity__r.Name FROM Opportunity_Product__c];
        FirstPage();
        return null;        
    }
    public Pagereference editOppProduct()
    {
        String oppProductid= ApexPages.currentPage().getParameters().get('oppProductid'); 
        return new PageReference('https://dholu-dev-ed.my.salesforce.com/'+oppProductid+'/e');
    }
    
    public Pagereference deleteOppProduct()
    {
        String oppProductid= ApexPages.currentPage().getParameters().get('oppProductid');
        Opportunity_Product__c oppProduct = [SELECT ID FROM Opportunity_Product__c WHERE ID =:oppProductid LIMIT 1];
        if(oppProduct !=null){
            DELETE oppProduct;
        }
        return null;
    }
    
    public PageReference LastPage() {
        obj = new List<Opportunity_Product__c>();
        count = totalcount;
        for(Integer j=0;j!=5;j++){
            if(count-1==0){break;}
            else{obj.add(objs[--count]);}
        }
        starting = count;
        return null;
    }
    public PageReference FirstPage() {
        obj = new List<Opportunity_Product__c>();
        starting = 1;
        count=-1;
        if(totalcount>count){
            for(Integer j=0;j!=5;j++){
                if(count+1==objs.size()){break;}
                else{obj.add(objs[++count]);}
            }
            ending = count+1;
        }
        return null;
    }
    
    public PageReference Previous() { 
        if(count>5){
            obj = new List<Opportunity_Product__c>();
            ending = count-1;
            for(Integer j=0;j!=5;j++){
                if(count-1==0){break;}
                else{obj.add(objs[--count]);}
            }
            starting = count;
        }
        return null;
    }
    
    public PageReference Next() {
        if(totalcount>count){
            starting = count+2;
            obj = new List<Opportunity_Product__c>();
            for(Integer j=0;j!=5;j++){
                if(count+1==objs.size()){++count;break;}
                else{obj.add(objs[++count]);}
            }
            ending = count;
        }
        return null;
    }
    public PageReference Search() {
        objs = [SELECT Name, Active__c, Primary__c, Opportunity__r.Name FROM Opportunity_Product__c WHERE Opportunity__r.Name LIKE :search + '%'];
        totalCount = objs.size();
        FirstPage();
        return null;
    }
}