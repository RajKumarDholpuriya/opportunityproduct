@isTest
public class OpporunityProduct_trg_Test {
    @testSetup static void declare() {
        Opportunity opp = new Opportunity(Name='Opp 1', Discount_Percent__c=1, StageName='Prospecting', CloseDate= Date.today());
        insert opp;
        
        Opportunity_Product__c oppProduct1 = new Opportunity_Product__c
            (Name='product 1', Active__c=true, Primary__c=true, Opportunity__c=opp.id);
        insert oppProduct1;
        
        System.assertEquals(true, oppProduct1.Primary__c);
            
        Opportunity_Product__c oppProduct2 = new Opportunity_Product__c
            (Name='product 1', Active__c=true, Primary__c=true, Opportunity__c=opp.id);
        insert oppProduct2;
        
        oppProduct2 = [SELECT Primary__c FROM Opportunity_Product__c where id = :oppProduct2.id LIMIT 1];
        System.assertEquals(false, oppProduct2.Primary__c);
    }
    
    @isTest static void check2(){
        Opportunity_Product__c oppProduct1 = [SELECT id, Opportunity__c,Primary__c from Opportunity_Product__c where Primary__c=true limit 1];
        
        System.assert(oppProduct1!=null);
        
        Opportunity_Product__c oppProduct2 = new Opportunity_Product__c
            (Name='product 3', Active__c=true, Primary__c=true, Opportunity__c=oppProduct1.Opportunity__c);
        insert oppProduct2;
        
        //System.assertEquals(false, oppProduct2.Primary__c);
        
        oppProduct2.Primary__c = true;
        update oppProduct2;
                
        oppProduct1.Primary__c = false;
        oppProduct2.Primary__c = true;
        
        List<Opportunity_Product__c> forUpdation = new List<Opportunity_Product__c>();
        forUpdation.add(oppProduct1);
        forUpdation.add(oppProduct2);
        
        update forUpdation;
        
        System.assertEquals(false, oppProduct1.Primary__c);
        
        Opportunity opp2 = new Opportunity
            (Name='Opp 1', Discount_Percent__c=1, StageName='Prospecting', CloseDate= Date.today());
        insert opp2;
        
        Opportunity_Product__c oppProduct3 = new Opportunity_Product__c
            (Name='pro 1', Active__c=true, Primary__c=false, Opportunity__c=opp2.id);
        insert oppProduct3;
        
        oppProduct3.Primary__c = true;
        update oppProduct3;
        
        System.assertEquals(true, oppProduct3.Primary__c);
    }
    
    @isTest static void check3(){
        Opportunity opp2 = new Opportunity(Name='Opp 1', Discount_Percent__c=1, StageName='Prospecting', CloseDate= Date.today());
        INSERT opp2;
        
        List<Opportunity_Product__c> oppProducts = new List<Opportunity_Product__c>();
        
        for(Integer i=1;i<10;i++){
        	Opportunity_Product__c oppProduct = new Opportunity_Product__c(Name='product 3', Active__c=true, Primary__c=true, Opportunity__c=opp2.id);
            oppProducts.add(oppProduct);
        }
        
        INSERT oppProducts;
        
        Opportunity_Product__c oppProduct = 
            [SELECT ID, Primary__c FROM Opportunity_Product__c WHERE Opportunity__c = :opp2.id AND Primary__c = true LIMIT 1];
        oppProduct.Primary__c = false;
        
        
        List<Opportunity_Product__c> forUpdation = 
            [SELECT ID, Primary__c FROM Opportunity_Product__c WHERE Opportunity__c = :opp2.id AND Primary__c = false];
        for(Opportunity_Product__c one : forUpdation){
            one.Primary__c = true;
        }
        
        forUpdation.add(oppProduct);
        UPDATE forUpdation;
        
        
    }
}