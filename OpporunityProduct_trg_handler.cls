public class OpporunityProduct_trg_handler {
    
    public static void upsertOpportunityProduct(List<Opportunity_Product__c> productOpp, Boolean isUpdate){
        Map<ID, Opportunity_Product__c> oppProducts = new Map<ID, Opportunity_Product__c>();
        for(Opportunity_Product__c productOne : productOpp){
            oppProducts.put(productOne.Opportunity__c, productOne);
        }
        
        Map<ID, Opportunity_Product__c> oldProductOpps = new Map<ID, Opportunity_Product__c>();
        
        for(Opportunity_Product__c oppProduct : [SELECT ID, Opportunity__c,Primary__c FROM Opportunity_Product__c WHERE Opportunity__c IN :oppProducts.keySet() AND Primary__c=true]){
            oldProductOpps.put(oppProduct.Opportunity__c, oppProduct);
        }
        
        if(!isUpdate){
            for(Opportunity_Product__c productOne : productOpp){
                if(oldProductOpps.containsKey(productOne.Opportunity__c)){
                    productOne.Primary__c = false;
                }
                else{
                    if(productOne.Primary__c == true){
                        oldProductOpps.put(productOne.Opportunity__c, productOne);
                    }
                }
            }
        }
        else
        {
            Map<ID, Opportunity_Product__c> updatedOpps = new Map<ID, Opportunity_Product__c>();
            for(Opportunity_Product__c productOne : productOpp){
                updatedOpps.put(productOne.id, productOne);
            }
            for(Opportunity_Product__c productOne : productOpp){
                if(oldProductOpps.containsKey(productOne.Opportunity__c)){
                    Opportunity_Product__c one = oldProductOpps.get(productOne.Opportunity__c);
                    Opportunity_Product__c two = updatedOpps.get(one.id);
                    
                    if(two==null || (two.Primary__c && productOne.id!=two.id)){
                        productOne.Primary__c = false;
                    }
                    else{oldProductOpps.put(productOne.Opportunity__c, productOne);}
                }
                else{
                    if(productOne.Primary__c == true){
                        oldProductOpps.put(productOne.Opportunity__c, productOne);
                    }
                }
            }
        }
    }	
}